'''

Here comes a simplified solution to the print a calender program

It tried to brake down the solution to it's individual pieces to understand it better and also I provided the steps
that I did and the code form simple to gradually more complex as I add the new elements.

It help me a great deal understand this better and if there another one out there also struggling with this problem
I hope this post can help a little.

This is my solution and explanation the way it help me personally understand the problem myself and reproduce the solution
on my own cause the many indexes and iterators sure did confuse me at a first and at a second...
ok sure at a third glace too.

So the aftermath seems to be that with m and d we are creating a moving indexes
     or a more accurate way to put it would probably be iterating through indexes.


        for m in range(0, 12):
            print(months[m])
                                    Iterator A

        for d in range(0, 7):
            print(weekdays[d])
                                    Iterator B

        for m in range(0, 12):
            print(months[m])
            for d in range(0, 7):
                print(weekdays[d])
                                    Iterator A & B combined

This is the point where the 7 days of the week are printed for each 12 months of the year.

    So we iterated through every month in our months list and for each month we iterated through every week day
    Now that gives us 7 days for each of the 12 months.
    That is because we iterated through a 7 long range.


But obviously that is wrong or incomplete for our program's sake.


____________________________________________________________________________



I order to iterate and print a day for each of the maximum range of days in a month we need to make some changes.

So instead of range 0 to 7 we want range 0-30 or range 0-31 or range 0-28 or 0-29 if it is a leap year February.

That is why we created a list for every months maximum days in a non-leap-year and also for a leap year.


        leap = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


        nonYear = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


        for m in range(0, 12):
            print(months[m])
            for d in range(0, leap[m]):
                print(weekdays[d])

                                        Iterator A, B & leap(list) combined
                                           (indexing based on leap list)


However if we iterated to the maximum of that month with our conventional week list which have only the seven days
the iteration would go out of range and through us an error.

            Traceback (most recent call last):
              File "/Users/PycharmProjects/blablablabla.py", line 126, in <module>
                print(weekdays[d])
            IndexError: list index out of range

That is why we came up with the convention of the iterator C

        day += 1
        if day == 7:
            day = 0
                        Iterator C

What is special with iterator C as we see here is that it never goes out of range 31 or 30 or 28 simply because
it resets every time it reaches the maximum of 7 which are exactly the day in our weeks list.
Now our week list does not need to go out of range any more!



        for m in range(0, 12):
            print(months[m])
            for d in range(0, leap[m]):
                print(weekdays[day])
                day += 1
                if day == 7:
                    day = 0

                                        Iterator A, B, C and leap combined
                                        leap(list)
                                        C (reset day iterator)
                                        (indexing based on leap list)


In the end we want the day number in front of the day in order to print it out in a more calender-looking form

In order to do that we got to print the d

That would look this this:

        for m in range(0, 12):
            print(months[m])
            for d in range(0, leap[m]):
                print(str(d) + weekdays[day])
                day += 1
                if day == 7:
                    day = 0

Now if your run this you would get a result looking something like this:

    (On str(d) the str part is added to convert the integer type d to an string in order to be concatenated
    and printed together with the weekdays strings)

                January
                0Monday
                1Tuesday
                2Wednesday
                ...
                ...
                ...
                28Monday
                29Tuesday
                30Wednesday

Because d runs from range 0 up to the given months max day lets say 31 one for January. It prints out 0 for Monday and
for the first day of any new given month. Also it goes always up until BUT NOT INCLUDING the last day of the max days
of the month. That is because range does not use the last digit of the second argument.
So for example in this code number 10 will not
get printed.

        for x in range(1, 10):
            print(x)

That also means that we get incorrect both the first but also the last day of the month too. January is supose to have
31 say not 30.

The solution is simply to add d + 1 on our code so that every time d gets printed it will display the right value.

for m in range(0, 12):
    print(months[m])
    for d in range(0, leap[m]):
        print(str(d + 1) + weekdays[day])
        day += 1
        if day == 7:
            day = 0

Now that is pretty much it and the rest of the '   ' in this line of code:

print('   ' + str(d + 1) + '   ' + weekdays[day])



is to make the print statements look cleaner and clearer

for m in range(0, 12):
    print(months[m])
    for d in range(0, leap[m]):
        print('   ' + str(d + 1) + '   ' + weekdays[day])
        day += 1
        if day == 7:
            day = 0

There are other methods being used on the original code like .capitalize or .lower or
if svar == "ja":
    maxdagar[1] = 29

But that for me the code included here is the bare minimum to make the program run and to be easily understood
for beginner programmers.


So this is the bare-bones solution without the added code.

The non-leap list is included in case somebody wants to have it there.


Hope you enjoyed it.

'''


#nonYear = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November',
          'December']

weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']


leap = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


day = 0


for m in range(0, 12):
    print(months[m])
    for d in range(0, leap[m]):
        print('   ' + str(d + 1) + '   ' + weekdays[day])
        day += 1
        if day == 7:
            day = 0
